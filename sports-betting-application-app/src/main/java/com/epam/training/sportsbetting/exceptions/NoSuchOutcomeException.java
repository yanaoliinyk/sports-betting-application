package com.epam.training.sportsbetting.exceptions;

/**
 * NoSuchOutcomeException.
 */
public class NoSuchOutcomeException extends ValidationException {

    /**
     * @param message with detailed exception info
     */
    public NoSuchOutcomeException(final String message) {
        super(message);
    }
}
