package com.epam.training.sportsbetting.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbetting.exceptions.BalanceNotSpecifiedException;
import com.epam.training.sportsbetting.exceptions.NegativeAmountException;
import com.epam.training.sportsbetting.exceptions.NoSuchBetException;
import com.epam.training.sportsbetting.exceptions.NoSuchOutcomeException;
import com.epam.training.sportsbetting.exceptions.ValidationException;

/**
 * validation methods.
 */
@Component
public final class ValidationHelper {
    @Autowired
    private Data.DataResolver dataResolver;

    /**
     * @param bet     index
     * @param outcome index
     * @param money   amount
     * @throws ValidationException in case of invalid data
     */
    public void validateBetInput(final int bet,
                                 final int outcome,
                                 final long money) throws ValidationException {
        if (dataResolver.getPlayer() == null) {
            throw new BalanceNotSpecifiedException(
                    "Balance was not specified for current player");
        }
        if (bet >= dataResolver.getSportEvents().size()) {
            throw new NoSuchBetException("incorrect bet number");
        }
        if (outcome >= dataResolver.getSportEvents()
                .get(dataResolver.getSportEventIndex())
                .getBets().get(bet).getOutcomes().size()) {
            throw new NoSuchOutcomeException("incorrect outcome number");
        }
        if (money < 0) {
            throw new NegativeAmountException("amount cannot be less than 0");
        }
    }
}
