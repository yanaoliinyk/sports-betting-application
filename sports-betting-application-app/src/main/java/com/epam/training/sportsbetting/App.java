package com.epam.training.sportsbetting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;

/**
 * app main class.
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.epam.training.sportsbetting"})
public final class App {

    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    private App() {
    }

    /**
     * @param args entry point
     */
    public static void main(final String[] args) {
        String[] disabledCommands = {"--spring.shell.command.quit.enabled=false"};
        String[] fullArgs = StringUtils
                .concatenateStringArrays(args, disabledCommands);
        LOG.info("starting app");
        LOG.trace("sisi");
        SpringApplication.run(App.class, fullArgs);
    }
}
