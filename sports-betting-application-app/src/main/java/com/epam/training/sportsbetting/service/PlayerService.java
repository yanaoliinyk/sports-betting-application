package com.epam.training.sportsbetting.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.epam.training.sportsbettingdomain.domain.user.Player;
import com.epam.training.sportsbettingdomain.domain.wager.Wager;

/**
 * player methods.
 */
@Component
public class PlayerService {

    private static final Logger LOG = LoggerFactory.getLogger(PlayerService.class);

    /**
     * @param wager object with money amount
     * @return true if wager is valid(amount is less than player balance)
     * , false otherwise
     */
    public boolean isWagerValid(final Wager wager) {
        return wager.getAmount() <= wager.getPlayer().getBalance();
    }

    private void decreasePlayerBalance(final Player player,
                                       final long amount) throws Exception {
        long balance = player.getBalance();
        if (balance - amount <= 0) {
            LOG.error("user had not enough money to bet: " + player.getName() + " had " + player.getBalance()
                    + ", bet amount: " + amount);
            throw new Exception("not enough money, your balance is" + balance);
        }
        player.setBalance(balance - amount);
        LOG.info("new balance set to player: " + player.getName() + " , " + player.getBalance());
    }

    private long increasePlayerBalance(final Player player,
                                       final long amount,
                                       final double odd) {
        long wonAmount = (long) (amount * odd);
        player.setBalance(player.getBalance() + wonAmount);
        return wonAmount - amount;
    }

    /**
     * @param wager  which is being processed
     * @param amount of money, that was put
     */
    public void recalculatePlayerBalance(final Wager wager,
                                         final long amount) {
        try {
            decreasePlayerBalance(wager.getPlayer(), amount);
            if (wager.isWin()) {
                long profit = increasePlayerBalance(wager.getPlayer(),
                        amount, wager.getOutcomeOdd().getOdd());
                wager.setProfit(profit);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }
}
