package com.epam.training.sportsbetting.exceptions;

/**
 * ValidationException.
 */
public class ValidationException extends Exception {

    /**
     * @param message with detailed exception info
     */
    public ValidationException(final String message) {
        super(message);
    }
}
