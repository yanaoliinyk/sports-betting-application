package com.epam.training.sportsbetting.exceptions;

/**
 * NegativeAmountException.
 */
public class NegativeAmountException extends ValidationException {

    /**
     * @param message with detailed exception info
     */
    public NegativeAmountException(final String message) {
        super(message);
    }
}
