package com.epam.training.sportsbetting.exceptions;

/**
 * JsonCouldNotBeParsedException.
 */
public class JsonCouldNotBeParsedException extends ValidationException {
    /**
     * @param message with detailed exception info
     */
    public JsonCouldNotBeParsedException(String message) {
        super(message);
    }
}
