package com.epam.training.sportsbetting.ui;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbetting.service.OutcomeService;

import java.util.List;

public final class StringFormattingHelper {

    private StringFormattingHelper() {
    }

    /**
     * @param strings list of strings, that should be formatted
     * @return formatted list of given strings
     */
    public static String getListToPrint(final List<String> strings) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strings.size(); i++) {
            sb.append(i).append(". ").append(strings.get(i)).append("\n");
        }
        return sb.toString();
    }

    /**
     * @param bets , that should be formatted as list
     * @return formatted list of bets
     */
    public static String getBetList(final List<Bet> bets) {
        StringBuilder betList = new StringBuilder();
        int i = 0;
        for (Bet bet : bets) {
            betList.append(i).append(". ").append(bet.toString())
                    .append("\n").append(getBetOutcomesList(bet)).append("\n");
            i++;
        }
        return betList.toString();
    }

    private static String getBetOutcomesList(final Bet bet) {
        StringBuilder outcomesList = new StringBuilder();
        int i = 0;
        for (Outcome outcome : bet.getOutcomes()) {
            outcomesList.append(i).append(") ")
                    .append(outcome.toString())
                    .append(", odd = ")
                    .append(OutcomeService
                            .getActualOutcomeOdd(outcome).getOdd())
                    .append("\n");
            i++;
        }
        return outcomesList.toString();
    }

    /**
     * @param sportEvent title
     * @param bet        object, that was used
     * @param outcome    that was chosen by player
     * @param money      that were put on bet
     * @param odd        for current outcome
     * @return formatted bet answer
     */
    public static String getBetAnswer(final String sportEvent,
                                      final Bet bet,
                                      final int outcome,
                                      final long money,
                                      final double odd) {
        return "Thanks!" + " You bet " + money
                + new Data.DataResolver().getPlayer().getCurrency()
                + " on " + sportEvent + "\n"
                + "bet: " + bet.toString() + "\n"
                + "outcome: " + bet.getOutcomes().get(outcome).toString() + "\n"
                + "with odd: " + odd + "\n";
    }
}
