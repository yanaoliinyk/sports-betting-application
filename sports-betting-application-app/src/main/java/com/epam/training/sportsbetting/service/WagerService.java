package com.epam.training.sportsbetting.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbettingdomain.domain.wager.Wager;

/**
 * wager methods.
 */
@Component
public class WagerService {

    private static final Logger LOG = LoggerFactory.getLogger(WagerService.class);

    @Autowired
    private PlayerService playerService;

    @Autowired
    private Data.DataResolver dataResolver;

    /**
     * @param bet     index
     * @param outcome index
     * @param money   amount, put on bet
     * @return fully constructed wager object
     */
    public Wager createWager(final int bet,
                             final int outcome,
                             final long money) {
        return new Wager.Builder()
                .withOutcomeOdd(OutcomeService.getActualOutcomeOdd(
                        dataResolver.getSportEvents().get(
                                dataResolver.getSportEventIndex())
                                .getBets().get(bet)
                                .getOutcomes().get(outcome)))
                .ofPlayer(dataResolver.getPlayer())
                .withAmount(money)
                .withCurrency(dataResolver.getPlayer().getCurrency())
                .build();
    }

    /**
     * @param wager object to be processed
     */
    public void processWager(final Wager wager) {
        wager.getPlayer().getWagers().add(wager);
        if (playerService.isWagerValid(wager)) {
            wager.setWin(isWagerWin(wager));
            playerService.recalculatePlayerBalance(wager, wager.getAmount());
            wager.setProcessed(true);
        } else {
            LOG.error("Not enough money to bet");
        }
    }

    private boolean isWagerWin(final Wager wager) {
        return wager.getOutcomeOdd().getOutcome()
                .getWagerCheckStrategy().isWagerWin(wager);
    }

    /**
     * @param wager object
     * @return win amount for the wager specified
     */
    public String getWagerWinAmount(final Wager wager) {
        return wager.getProfit() + wager.getCurrency();
    }
}
