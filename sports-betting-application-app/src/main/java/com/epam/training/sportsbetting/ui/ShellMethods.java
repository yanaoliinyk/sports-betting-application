package com.epam.training.sportsbetting.ui;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.ExitRequest;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbetting.datapreparation.DataParser;
import com.epam.training.sportsbetting.exceptions.ValidationException;
import com.epam.training.sportsbetting.service.WagerService;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.epam.training.sportsbettingdomain.domain.user.Player;
import com.epam.training.sportsbettingdomain.domain.wager.Wager;


/**
 * console commands processing.
 */
@ShellComponent
public final class ShellMethods {
    @Autowired
    private WagerService wagerService;

    @Autowired
    private DataParser dataParser;

    @Autowired
    private ValidationHelper validationHelper;

    private Data.DataResolver dataResolver = new Data.DataResolver();

    /**
     * @param name          for player name
     * @param accountNumber for player account number
     * @param balance       for player balance
     * @param currency      for suggested currency
     * @param birthDate     for birth date
     * @return message of successful player addition.
     */
    @ShellMethod(key = {"set-user-info", "info"},
            value = "set user information(name, account number, balance, "
                    + "currency(default = UAH), birth date(empty by default))")
    public String setPlayerInfo(
            final String name,
            final String accountNumber,
            final long balance,
            final @ShellOption(defaultValue = "UAH") String currency,
            final @ShellOption(defaultValue = "") String birthDate) {
        Player player = new Player.Builder()
                .withName(name)
                .withAccountNumber(accountNumber)
                .withBalance(balance)
                .withCurrency(currency)
                .withDateOfBirth(birthDate)
                .build();

        dataResolver.setPlayer(player);
        return "Player added";
    }

    /**
     * @return current session player info.
     */
    @ShellMethod(key = "get-my-info",
            value = "Returns user info")
    public String getPlayerInfo() {
        if (dataResolver.getPlayer() != null) {
            return dataResolver.getPlayer().toString();
        } else {
            return "Player was not specified yet";
        }
    }

    /**
     * @return list of sport events available for betting.
     */
    @ShellMethod(key = "get-events-list",
            value = "get list of events")
    public String getEventsList() {
        return StringFormattingHelper.getListToPrint(dataResolver
                .getSportEvents()
                .stream()
                .map(SportEvent::getTitle)
                .collect(Collectors.toList()));
    }

    /**
     * @param event for sport event index
     * @return list of available bets with outcomes with odds.
     */
    @ShellMethod(key = "get-bets",
            value = "shows all bets for chosen sport event")
    public String getBetList(final int event) {
        if (event >= dataResolver.getSportEvents().size()) {
            return "there is not sport event with this number";
        }
        System.out.println(dataResolver
                .getSportEvents().get(event).getTitle() + "\n");
        dataResolver.setSportEventIndex(event);
        return StringFormattingHelper.getBetList(dataResolver
                .getSportEvents().get(event).getBets());
    }

    /**
     * @param bet     for bet index
     * @param outcome for outcome index
     * @param money   for betting amount
     * @return info about bet result.
     */
    @ShellMethod(key = "bet",
            value = "Bet on sport event and outcome")
    public String betOnGame(final int bet, final int outcome, final long money) {
        try {
            int sportEventIndex = dataResolver.getSportEventIndex();
            validationHelper.validateBetInput(bet, outcome, money);
            Wager wager = wagerService.createWager(bet, outcome, money);
            wagerService.processWager(wager);
            String answer = StringFormattingHelper.getBetAnswer(
                    dataResolver.getSportEvents().get(sportEventIndex).getTitle(),
                    dataResolver.getSportEvents().get(sportEventIndex).getBets().get(bet), outcome, money, wager.getOutcomeOdd().getOdd());
            if (wager.isProcessed() && wager.isWin()) {
                return answer + "you won " + wagerService.getWagerWinAmount(wager);
            } else if (wager.isProcessed()) {
                return answer + "you lost";
            } else {
                return "wager could not be processed";
            }
        } catch (ValidationException e) {
            return e.getMessage();
        }
    }

    /**
     * @return message with player balance
     */
    @ShellMethod(key = "balance",
            value = "get my current balance")
    public String getCurrentBalance() {
        if (dataResolver.getPlayer() == null) {
            return "Player was not specified yet";
        } else {
            return "Your balance is "
                    + dataResolver.getPlayer().getBalance()
                    + dataResolver.getPlayer().getCurrency();
        }
    }

    /**
     * quits app with balance output.
     */
    @ShellMethod(key = "quit",
            value = "quit")
    public void quit() {
        if (dataResolver.getPlayer() == null) {
            System.out.println("you didn't start betting");
        } else {
            System.out.println(getCurrentBalance());
        }
        throw new ExitRequest();
    }
}
