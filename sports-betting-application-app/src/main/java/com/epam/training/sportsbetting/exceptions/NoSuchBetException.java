package com.epam.training.sportsbetting.exceptions;

/**
 * NoSuchBetException.
 */
public class NoSuchBetException extends ValidationException {

    /**
     * @param message with detailed exception info
     */
    public NoSuchBetException(final String message) {
        super(message);
    }
}
