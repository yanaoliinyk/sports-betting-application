package com.epam.training.sportsbetting.service;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbettingdomain.domain.outcome.OutcomeOdd;

/**
 * outcome methods.
 */
@Component
public final class OutcomeService {

    private OutcomeService() {
    }

    /**
     * @param outcome object
     * @return odd, that is actual in time
     */
    public static OutcomeOdd getActualOutcomeOdd(final Outcome outcome) {
        OutcomeOdd outcomeOddResult =  outcome.getOutcomeOdds()
                .get(outcome.getOutcomeOdds().size() - 1);
        for (OutcomeOdd outcomeOdd : outcome.getOutcomeOdds()) {
            if (outcomeOdd.getTo() == null
                    || outcomeOdd.getTo().isAfter(LocalDateTime.now())) {
                outcomeOddResult = outcomeOdd;
                break;
            }
        }
        return outcomeOddResult;
    }
}
