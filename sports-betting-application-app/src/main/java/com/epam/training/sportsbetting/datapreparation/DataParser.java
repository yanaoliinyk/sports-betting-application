package com.epam.training.sportsbetting.datapreparation;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.epam.training.sportsbetting.exceptions.JsonCouldNotBeParsedException;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * class parses json file with data.
 */
@Component
public final class DataParser {
    private static final Logger LOG = LoggerFactory.getLogger(DataParser.class);

    /**
     * path to json file.
     */
    private static final String JSON_FILE_PATH
            = "src/main/resources/events.json";

    /**
     * Object Mapper.
     */
    private static ObjectMapper mapper = new ObjectMapper();

    private DataParser() {
    }

    /**
     * @return list of sport events from Json file
     * @throws JsonCouldNotBeParsedException if file could not be parsed
     */
    public static List<SportEvent> getSportEvents() throws JsonCouldNotBeParsedException {
        try {
            LOG.info("start file parsing");
            LOG.debug("debug start file parsing");
            mapper.findAndRegisterModules();
            return Arrays.asList(mapper.readValue(
                    new File(JSON_FILE_PATH), SportEvent[].class));
        } catch (IOException e) {
            LOG.error(e.getMessage());
            LOG.trace("everything is bad");
            throw new JsonCouldNotBeParsedException("error while parsing json file");
        }
    }
}

