package com.epam.training.sportsbetting.exceptions;

/**
 * balance not specified exception.
 */
public class BalanceNotSpecifiedException extends ValidationException {

    /**
     * @param message with detailed exception info
     */
    public BalanceNotSpecifiedException(final String message) {
        super(message);
    }
}
