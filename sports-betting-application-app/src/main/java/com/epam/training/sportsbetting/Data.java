package com.epam.training.sportsbetting;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.training.sportsbetting.datapreparation.DataParser;
import com.epam.training.sportsbetting.exceptions.JsonCouldNotBeParsedException;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.epam.training.sportsbettingdomain.domain.user.Player;

/**
 * Data static class.
 */
public final class Data {
    /**
     * Data instance.
     */
    private static Data data;

    /**
     * current sport event index, that takes part in betting.
     */
    private int sportEventIndex;
    /**
     * current session player.
     */
    private Player player;
    /**
     * list of all available sport events.
     */
    private List<SportEvent> sportEvents;

    private Data() {
    }

    private static Data getData() {
        if (data == null) {
            data = new Data();
            try {
                data.sportEvents = DataParser.getSportEvents();
            } catch (JsonCouldNotBeParsedException e) {

            }
        }
        return data;
    }

    /**
     * class to communicate with data.
     */
    @Component
    public static class DataResolver {
        /**
         * @return Data instance.
         */
        public Data getData() {
            return Data.getData();
        }

        /**
         * @return list of all available sport events.
         */
        public List<SportEvent> getSportEvents() {
            return getData().sportEvents;
        }

        /**
         * @param sportEvents is list of all available sport events.
         */
        public void setSportEvents(final List<SportEvent> sportEvents) {
            getData().sportEvents = sportEvents;
        }

        /**
         * @return current session player.
         */
        public Player getPlayer() {
            return getData().player;
        }

        /**
         * @param player is current session player.
         */
        public void setPlayer(final Player player) {
            getData().player = player;
        }

        /**
         * @return current sport event index, that takes part in betting.
         */
        public int getSportEventIndex() {
            return getData().sportEventIndex;
        }

        /**
         * @param sportEventIndex of current sport event,
         *                        that takes part in betting.
         */
        public void setSportEventIndex(final int sportEventIndex) {
            getData().sportEventIndex = sportEventIndex;
        }
    }
}
