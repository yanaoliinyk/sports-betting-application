package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbettingdomain.domain.outcome.OutcomeOdd;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertSame;

public class OutcomeServiceTest {

    private Outcome outcome = new Outcome() {
        @Override
        public void setWagerCheckStrategy() {

        }
    };

    @Test
    public void shouldReturnActualOutcomeOddWhenToDateTimeIsSet(){
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        outcomeOdd.setTo(LocalDateTime.of(2010,1,1,1,1));
        OutcomeOdd outcomeOddActual = new OutcomeOdd();
        outcomeOddActual.setTo(LocalDateTime.of(2021,1,1,1,1));
        outcome.setOutcomeOdds(Arrays.asList(outcomeOdd, outcomeOddActual));

        assertSame(outcomeOddActual, OutcomeService.getActualOutcomeOdd(outcome));
    }

    @Test
    public void shouldReturnActualOutcomeOddWhenToDateTimeIsNull(){
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        outcomeOdd.setTo(LocalDateTime.of(2010,1,1,1,1));
        OutcomeOdd outcomeOddActual = new OutcomeOdd();
        outcome.setOutcomeOdds(Arrays.asList(outcomeOdd, outcomeOddActual));

        assertSame(outcomeOddActual, OutcomeService.getActualOutcomeOdd(outcome));
    }

    @Test
    public void shouldReturnLastActualOutcomeOdd(){
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        outcomeOdd.setTo(LocalDateTime.of(2010,1,1,1,1));
        outcome.setOutcomeOdds(Collections.singletonList(outcomeOdd));

        assertSame(outcomeOdd, OutcomeService.getActualOutcomeOdd(outcome));
    }

}
