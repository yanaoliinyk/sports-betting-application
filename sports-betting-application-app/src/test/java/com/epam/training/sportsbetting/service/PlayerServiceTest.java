package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbettingdomain.domain.outcome.OutcomeOdd;
import com.epam.training.sportsbettingdomain.domain.user.Player;
import com.epam.training.sportsbettingdomain.domain.wager.Wager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerServiceTest {

    private PlayerService playerService = new PlayerService();

    @Test
    public void shouldReturnWagerNotValid() {
        Player player = new Player();
        player.setBalance(100);
        Wager wager = new Wager.Builder().ofPlayer(player).withAmount(200).build();

        assertFalse(playerService.isWagerValid(wager));
    }

    @Test
    public void shouldReturnWagerValid() {
        Player player = new Player();
        player.setBalance(100);
        Wager wager = new Wager.Builder().ofPlayer(player).withAmount(20).build();

        assertTrue(playerService.isWagerValid(wager));
    }

    @Test
    public void shouldNotRecalculatePlayerBalanceIfNotEnoughMoney() {
        Player player = new Player();
        player.setBalance(100);
        Wager wager = new Wager.Builder().ofPlayer(player).build();

        playerService.recalculatePlayerBalance(wager, 200);
        assertEquals(100, player.getBalance());
    }

    @Test
    public void shouldIncreasePlayerBalanceIfWon() {
        Player player = new Player();
        player.setBalance(100);
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        outcomeOdd.setOdd(1.5);
        Wager wager = new Wager.Builder().ofPlayer(player).withOutcomeOdd(outcomeOdd).build();
        wager.setWin(true);

        playerService.recalculatePlayerBalance(wager, 50);
        assertEquals(125, player.getBalance());
    }

    @Test
    public void shouldDecreasePlayerBalanceIfLost() {
        Player player = new Player();
        player.setBalance(100);
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        outcomeOdd.setOdd(1.5);
        Wager wager = new Wager.Builder().ofPlayer(player).withOutcomeOdd(outcomeOdd).build();
        wager.setWin(false);

        playerService.recalculatePlayerBalance(wager, 50);
        assertEquals(50, player.getBalance());
    }
}
