package com.epam.training.sportsbetting.ui;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.epam.training.sportsbettingdomain.domain.outcome.ScoresOutcome;
import com.epam.training.sportsbettingdomain.domain.outcome.WinnerOutcome;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.epam.training.sportsbettingdomain.domain.user.Player;
import com.epam.training.sportsbetting.exceptions.BalanceNotSpecifiedException;
import com.epam.training.sportsbetting.exceptions.NegativeAmountException;
import com.epam.training.sportsbetting.exceptions.NoSuchBetException;
import com.epam.training.sportsbetting.exceptions.NoSuchOutcomeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ValidationHelperTest {

    @InjectMocks
    private ValidationHelper instance = new ValidationHelper();

    @Mock
    private Bet betMock;

    @Mock
    private SportEvent sportEventMock;

    @Mock
    private Data.DataResolver dataResolverMock;

    @Test
    public void shouldThrowExceptionIfPlayerIsNull() {
        when(dataResolverMock.getPlayer()).thenReturn(null);
        assertThrows(BalanceNotSpecifiedException.class,
                () -> instance.validateBetInput(0, 0, 0));
    }

    @Test
    public void shouldThrowExceptionIfBetNumberIsBiggerThanBetsQuantity() {
        when(dataResolverMock.getPlayer()).thenReturn(new Player());
        when(dataResolverMock.getSportEvents()).thenReturn(Collections.singletonList(sportEventMock));
        when(sportEventMock.getBets()).thenReturn(Arrays.asList(new Bet(), new Bet()));

        assertThrows(NoSuchBetException.class,
                () -> instance.validateBetInput(4, 0, 0));
    }

    @Test
    public void shouldThrowExceptionIfOutcomeNumberIsBiggerThanOutcomeQuantity() {
        when(dataResolverMock.getPlayer()).thenReturn(new Player());
        when(dataResolverMock.getSportEvents()).thenReturn(Collections.singletonList(sportEventMock));
        when(sportEventMock.getBets()).thenReturn(Collections.singletonList(betMock));
        when(betMock.getOutcomes())
                .thenReturn(new ArrayList<>(Arrays.asList(new ScoresOutcome(), new WinnerOutcome())));

        assertThrows(NoSuchOutcomeException.class,
                () -> instance.validateBetInput(0, 6, 0));
    }

    @Test
    public void shouldThrowExceptionIfAmountIsNegative(){
        when(dataResolverMock.getPlayer()).thenReturn(new Player());
        when(dataResolverMock.getSportEvents()).thenReturn(Collections.singletonList(sportEventMock));
        when(sportEventMock.getBets()).thenReturn(Collections.singletonList(betMock));
        when(betMock.getOutcomes())
                .thenReturn(new ArrayList<>(Arrays.asList(new ScoresOutcome(), new WinnerOutcome())));

        assertThrows(NegativeAmountException.class,
                () -> instance.validateBetInput(0, 0, -100));
    }

    @Test
    public void shouldThrowNoExceptionIfDataIsValid() {
        when(dataResolverMock.getPlayer()).thenReturn(new Player());
        when(dataResolverMock.getSportEvents()).thenReturn(Collections.singletonList(sportEventMock));
        when(sportEventMock.getBets()).thenReturn(Collections.singletonList(betMock));
        when(betMock.getOutcomes())
                .thenReturn(new ArrayList<>(Arrays.asList(new ScoresOutcome(), new WinnerOutcome())));

        assertDoesNotThrow(() -> instance.validateBetInput(0, 0, 0));
    }

}
