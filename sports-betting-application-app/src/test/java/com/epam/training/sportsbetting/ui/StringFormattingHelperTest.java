package com.epam.training.sportsbetting.ui;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.epam.training.sportsbettingdomain.domain.bet.BetType;
import com.epam.training.sportsbettingdomain.domain.outcome.*;
import com.epam.training.sportsbettingdomain.domain.user.Player;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StringFormattingHelperTest {

    private Data.DataResolver dataResolver = new Data.DataResolver();

    private Bet getBet() {
        Bet b = new Bet();
        b.setBetType(BetType.GOALS_BET);
        b.setDescription("description");
        GoalsOutcome goalsOutcome = new GoalsOutcome(3);
        WinnerOutcome winnerOutcome = new WinnerOutcome("winner");
        ScoresOutcome scoresOutcome = new ScoresOutcome("player", 2);
        List<Outcome> outcomes = new ArrayList<>(Arrays.asList(goalsOutcome, winnerOutcome, scoresOutcome));
        for (Outcome o : outcomes) {
            OutcomeOdd odd = new OutcomeOdd();
            odd.setOdd(1.5);
            o.setOutcomeOdds(Collections.singletonList(odd));
        }
        b.setOutcomes(outcomes);

        return b;
    }

    private List<Bet> getTestedBetList() {
        return new ArrayList<>(Arrays.asList(getBet(), getBet()));
    }

    @Test
    public void shouldReturnFormattedList() {
        List<String> testedList = new ArrayList<>(Arrays.asList("1", "2", "3"));
        String expected = "0. 1\n1. 2\n2. 3\n";
        assertEquals(expected, StringFormattingHelper.getListToPrint(testedList));
    }

    @Test
    public void shouldReturnFormattedBetList() {
        List<Bet> bets = getTestedBetList();
        String expected = "0. Bet:GOALS_BET, description:description\n" +
                "0) goals=3, odd = 1.5\n" +
                "1) winner='winner', odd = 1.5\n" +
                "2) player: 'player', score=2, odd = 1.5\n" +
                "\n" +
                "1. Bet:GOALS_BET, description:description\n" +
                "0) goals=3, odd = 1.5\n" +
                "1) winner='winner', odd = 1.5\n" +
                "2) player: 'player', score=2, odd = 1.5\n" +
                "\n";

        assertEquals(expected, StringFormattingHelper.getBetList(bets));
    }

    @Test
    public void shouldReturnFormattedBetAnswer() {
        Player p = new Player();
        p.setCurrency("UAH");
        dataResolver.setPlayer(p);
        String expected = "Thanks! You bet 123UAH on sport event\n" +
                "bet: Bet:GOALS_BET, description:description\n" +
                "outcome: winner='winner'\n" +
                "with odd: 1.8\n";

        assertEquals(expected, StringFormattingHelper.getBetAnswer("sport event",
                getBet(), 1, 123, 1.8));
    }
}
