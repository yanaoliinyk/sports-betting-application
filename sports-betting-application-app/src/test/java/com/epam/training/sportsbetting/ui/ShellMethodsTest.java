package com.epam.training.sportsbetting.ui;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbettingdomain.domain.sportevent.FootballSportEvent;
import com.epam.training.sportsbettingdomain.domain.sportevent.TennisSportEvent;
import com.epam.training.sportsbettingdomain.domain.user.Player;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ShellMethodsTest {

    @InjectMocks
    private ShellMethods instance = new ShellMethods();

    private Data.DataResolver dataResolver = new Data.DataResolver();

    @Test
    public void shouldSetAllFieldsOfPlayerInfo() {
        instance.setPlayerInfo("name", "123", 123, "USD", "1989-11-13");
        Player player = dataResolver.getPlayer();

        assertAll(
                () -> assertEquals("name", player.getName()),
                () -> assertEquals("123", player.getAccountNumber()),
                () -> assertEquals(123, player.getBalance()),
                () -> assertEquals("USD", player.getCurrency()),
                () -> assertEquals(LocalDate.of(1989, 11, 13), player.getDateOfBirth()));
    }

    @Test
    public void shouldReturnFormattedPlayerInfo() {
        Player player = new Player.Builder()
                .withName("name")
                .withAccountNumber("123")
                .withBalance(123)
                .withCurrency("UAH")
                .withDateOfBirth("1996-11-12")
                .build();
        dataResolver.setPlayer(player);

        String expectedInfo = "Player{name='name', accountNumber='123', balance=123, currency='UAH', dateOfBirth=1996-11-12}";
        assertEquals(expectedInfo, instance.getPlayerInfo());
    }

    @Test
    public void shouldReturnErrorMessageIfPlayerIsNotSet() {
        dataResolver.setPlayer(null);
        String expectedInfo = "Player was not specified yet";
        assertEquals(expectedInfo, instance.getPlayerInfo());
    }

    @Test
    public void shouldReturnWarnMessageIfSportEventDoesNotExist() {
        dataResolver.setSportEvents(Arrays.asList(new TennisSportEvent(), new FootballSportEvent()));
        String expectedString = "there is not sport event with this number";

        assertEquals(expectedString, instance.getBetList(3));
    }

    @Test
    public void shouldReturnPlayerNotSetMessageWhenGetBalance() {
        dataResolver.setPlayer(null);

        assertEquals("Player was not specified yet", instance.getCurrentBalance());
    }

    @Test
    public void shouldReturnBalance() {
        dataResolver.setPlayer(new Player.Builder().withBalance(123).withCurrency("UAH").build());

        assertEquals("Your balance is 123UAH", instance.getCurrentBalance());
    }

}
