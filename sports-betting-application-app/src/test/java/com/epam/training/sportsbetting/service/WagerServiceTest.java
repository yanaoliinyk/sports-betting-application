package com.epam.training.sportsbetting.service;

import com.epam.training.sportsbetting.Data;
import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbettingdomain.domain.outcome.OutcomeOdd;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.epam.training.sportsbettingdomain.domain.user.Player;
import com.epam.training.sportsbettingdomain.domain.wager.Wager;
import com.epam.training.sportsbettingdomain.domain.wager.WagerCheckStrategy;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WagerServiceTest {

    @InjectMocks
    WagerService wagerService;

    @Mock
    PlayerService playerServiceMock;

    @Mock
    SportEvent sportEventMock;

    @Mock
    Bet betMock;

    @Mock
    Outcome outcomeMock;

    @Mock
    WagerCheckStrategy wagerCheckStrategyMock;

    @Mock
    Data.DataResolver dataResolver;

    @Test
    public void shouldNotProcessWagerIfNotValid() {
        when(playerServiceMock.isWagerValid(any())).thenReturn(false);

        Player player = new Player();
        player.setBalance(100);
        Wager wager = new Wager.Builder().ofPlayer(player).withAmount(500).build();
        wagerService.processWager(wager);

        assertAll(
                () -> assertFalse(wager.isWin()),
                () -> assertFalse(wager.isProcessed()));
    }

    @Test
    public void shouldProcessWagerIfValid() {
        when(outcomeMock.getWagerCheckStrategy()).thenReturn(wagerCheckStrategyMock);
        when(wagerCheckStrategyMock.isWagerWin(any())).thenReturn(true);
        when(playerServiceMock.isWagerValid(any())).thenReturn(true);

        Player player = new Player();
        player.setBalance(100);
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        outcomeOdd.setOutcome(outcomeMock);
        Wager wager = new Wager.Builder().ofPlayer(player).withAmount(50).withOutcomeOdd(outcomeOdd).build();
        wagerService.processWager(wager);

        assertAll(
                () -> assertTrue(wager.isWin()),
                () -> assertTrue(wager.isProcessed()));
    }

    @Test
    public void shouldReturnFormattedProfit(){
        Wager wager = new Wager.Builder().withCurrency("USD").build();
        wager.setProfit(100);

        assertEquals("100USD", wagerService.getWagerWinAmount(wager));
    }

    @Test
    public void shouldCreateWager(){
        OutcomeOdd outcomeOdd = new OutcomeOdd();
        Player player = new Player();
        player.setCurrency("USD");
        when(dataResolver.getSportEvents()).thenReturn(Collections.singletonList(sportEventMock));
        when(dataResolver.getSportEventIndex()).thenReturn(0);
        when(dataResolver.getPlayer()).thenReturn(player);
        when(sportEventMock.getBets()).thenReturn(Collections.singletonList(betMock));
        when(betMock.getOutcomes()).thenReturn(Collections.singletonList(outcomeMock));
        when(outcomeMock.getOutcomeOdds()).thenReturn(Collections.singletonList(outcomeOdd));

        Wager wager = wagerService.createWager(0, 0, 100);

        assertAll(
                ()->assertSame(outcomeOdd, wager.getOutcomeOdd()),
                ()->assertSame(player, wager.getPlayer()),
                ()->assertEquals(100, wager.getAmount()),
                ()->assertEquals("USD", wager.getCurrency())
        );
    }
}
