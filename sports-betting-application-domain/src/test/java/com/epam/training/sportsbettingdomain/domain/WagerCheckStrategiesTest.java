package com.epam.training.sportsbettingdomain.domain;

import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.epam.training.sportsbettingdomain.domain.outcome.GoalsOutcome;
import com.epam.training.sportsbettingdomain.domain.outcome.OutcomeOdd;
import com.epam.training.sportsbettingdomain.domain.outcome.ScoresOutcome;
import com.epam.training.sportsbettingdomain.domain.outcome.WinnerOutcome;
import com.epam.training.sportsbettingdomain.domain.sportevent.Result;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.epam.training.sportsbettingdomain.domain.wager.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WagerCheckStrategiesTest {

    private WagerCheckStrategy wagerCheckStrategy;

    @Mock
    private Result resultMock;

    @Mock
    private SportEvent sportEventMock;

    @Mock
    private Bet betMock;

    @Mock
    private OutcomeOdd outcomeOddMock;

    @Test
    public void shouldReturnWinGoalsWagerCheckStrategy(){
        wagerCheckStrategy = new GoalsWagerCheckStrategy();
        GoalsOutcome goalsOutcomeWin = new GoalsOutcome(1);
        GoalsOutcome goalsOutcomeActual = new GoalsOutcome(1);
        when(resultMock.getGoalsOutcome()).thenReturn(goalsOutcomeWin);
        when(sportEventMock.getResult()).thenReturn(resultMock);
        when(betMock.getSportEvent()).thenReturn(sportEventMock);
        goalsOutcomeActual.setBet(betMock);
        when(outcomeOddMock.getOutcome()).thenReturn(goalsOutcomeActual);

        Wager wager = new Wager.Builder().withOutcomeOdd(outcomeOddMock).build();

        assertTrue(wagerCheckStrategy.isWagerWin(wager));
    }

    @Test
    public void shouldReturnNotWinGoalsWagerCheckStrategy(){
        wagerCheckStrategy = new GoalsWagerCheckStrategy();
        GoalsOutcome goalsOutcomeWin = new GoalsOutcome(1);
        GoalsOutcome goalsOutcomeActual = new GoalsOutcome(2);
        when(resultMock.getGoalsOutcome()).thenReturn(goalsOutcomeWin);
        when(sportEventMock.getResult()).thenReturn(resultMock);
        when(betMock.getSportEvent()).thenReturn(sportEventMock);
        goalsOutcomeActual.setBet(betMock);
        when(outcomeOddMock.getOutcome()).thenReturn(goalsOutcomeActual);

        Wager wager = new Wager.Builder().withOutcomeOdd(outcomeOddMock).build();

        assertFalse(wagerCheckStrategy.isWagerWin(wager));
    }

    @Test
    public void shouldReturnWinWinnerWagerCheckStrategy(){
        wagerCheckStrategy = new WinnerWagerCheckStrategy();
        WinnerOutcome winnerOutcomeWin = new WinnerOutcome("winner1");
        WinnerOutcome winnerOutcomeActual = new WinnerOutcome("winner1");
        when(resultMock.getWinnerOutcome()).thenReturn(winnerOutcomeWin);
        when(sportEventMock.getResult()).thenReturn(resultMock);
        when(betMock.getSportEvent()).thenReturn(sportEventMock);
        winnerOutcomeActual.setBet(betMock);
        when(outcomeOddMock.getOutcome()).thenReturn(winnerOutcomeActual);

        Wager wager = new Wager.Builder().withOutcomeOdd(outcomeOddMock).build();

        assertTrue(wagerCheckStrategy.isWagerWin(wager));
    }

    @Test
    public void shouldReturnNotWinWinnerWagerCheckStrategy(){
        wagerCheckStrategy = new WinnerWagerCheckStrategy();
        WinnerOutcome winnerOutcomeWin = new WinnerOutcome("winner1");
        WinnerOutcome winnerOutcomeActual = new WinnerOutcome("winner2");
        when(resultMock.getWinnerOutcome()).thenReturn(winnerOutcomeWin);
        when(sportEventMock.getResult()).thenReturn(resultMock);
        when(betMock.getSportEvent()).thenReturn(sportEventMock);
        winnerOutcomeActual.setBet(betMock);
        when(outcomeOddMock.getOutcome()).thenReturn(winnerOutcomeActual);

        Wager wager = new Wager.Builder().withOutcomeOdd(outcomeOddMock).build();

        assertFalse(wagerCheckStrategy.isWagerWin(wager));
    }

    @Test
    public void shouldReturnWinScoreWagerCheckStrategy(){
        wagerCheckStrategy = new ScoreWagerCheckStrategy();
        ScoresOutcome scoresOutcomeWin1 = new ScoresOutcome("pl", 1);
        ScoresOutcome scoresOutcomeWin2 = new ScoresOutcome("pl1", 1);
        ScoresOutcome scoresOutcomeActual = new ScoresOutcome("pl", 1);
        when(resultMock.getScoresOutcomes()).thenReturn(Arrays.asList(scoresOutcomeWin1, scoresOutcomeWin2));
        when(sportEventMock.getResult()).thenReturn(resultMock);
        when(betMock.getSportEvent()).thenReturn(sportEventMock);
        scoresOutcomeActual.setBet(betMock);
        when(outcomeOddMock.getOutcome()).thenReturn(scoresOutcomeActual);

        Wager wager = new Wager.Builder().withOutcomeOdd(outcomeOddMock).build();

        assertTrue(wagerCheckStrategy.isWagerWin(wager));
    }

    @Test
    public void shouldReturnNotWinScoreWagerCheckStrategy(){
        wagerCheckStrategy = new ScoreWagerCheckStrategy();
        ScoresOutcome scoresOutcomeWin1 = new ScoresOutcome("pl", 2);
        ScoresOutcome scoresOutcomeWin2 = new ScoresOutcome("pl1", 1);
        ScoresOutcome scoresOutcomeActual = new ScoresOutcome("pl", 1);
        when(resultMock.getScoresOutcomes()).thenReturn(Arrays.asList(scoresOutcomeWin1, scoresOutcomeWin2));
        when(sportEventMock.getResult()).thenReturn(resultMock);
        when(betMock.getSportEvent()).thenReturn(sportEventMock);
        scoresOutcomeActual.setBet(betMock);
        when(outcomeOddMock.getOutcome()).thenReturn(scoresOutcomeActual);

        Wager wager = new Wager.Builder().withOutcomeOdd(outcomeOddMock).build();

        assertFalse(wagerCheckStrategy.isWagerWin(wager));
    }
}
