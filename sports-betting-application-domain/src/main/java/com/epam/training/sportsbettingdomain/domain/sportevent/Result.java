package com.epam.training.sportsbettingdomain.domain.sportevent;

import java.util.List;

import com.epam.training.sportsbettingdomain.domain.outcome.GoalsOutcome;
import com.epam.training.sportsbettingdomain.domain.outcome.ScoresOutcome;
import com.epam.training.sportsbettingdomain.domain.outcome.WinnerOutcome;

public class Result {

    /**
     * outcome that represents winner of game.
     */
    private WinnerOutcome winnerOutcome;
    /**
     * outcome that represents sum of goals during game.
     */
    private GoalsOutcome goalsOutcome;
    /**
     * outcome that represents scores of players during game.
     */
    private List<ScoresOutcome> scoresOutcomes;

    /**
     * @return outcome that represents winner of game.
     */
    public WinnerOutcome getWinnerOutcome() {
        return winnerOutcome;
    }

    /**
     * @return outcome that represents sum of goals during game.
     */
    public GoalsOutcome getGoalsOutcome() {
        return goalsOutcome;
    }

    /**
     * @return outcome that represents scores of players during game.
     */
    public List<ScoresOutcome> getScoresOutcomes() {
        return scoresOutcomes;
    }
}
