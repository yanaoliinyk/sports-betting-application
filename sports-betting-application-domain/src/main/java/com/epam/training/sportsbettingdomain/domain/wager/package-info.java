/**
 * Contains all domain classes connected with Wager:
 * *Wager class,
 * *WagerCheckStrategy class:
 * * *GoalsWagerCheckStrategy class,
 * * *ScoreWagerCheckStrategy class,
 * * *WinnerWagerCheckStrategy class.
 */
package com.epam.training.sportsbettingdomain.domain.wager;
