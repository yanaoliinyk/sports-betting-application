package com.epam.training.sportsbettingdomain.domain.user;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.training.sportsbettingdomain.domain.wager.Wager;

public class Player {
    private static final Logger LOG = LoggerFactory.getLogger(Player.class);

    /**
     * player name.
     */
    private String name;
    /**
     * player account number.
     */
    private String accountNumber;
    /**
     * player balance.
     */
    private long balance;
    /**
     * player currency.
     */
    private String currency;
    /**
     * date of birth.
     */
    private LocalDate dateOfBirth;
    /**
     * list of wagers, bet by player.
     */
    private List<Wager> wagers = new ArrayList<>();

    /**
     * @return balance
     */
    public long getBalance() {
        return balance;
    }

    /**
     * @param balance of player
     */
    public void setBalance(final long balance) {
        this.balance = balance;
    }

    /**
     * @return currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency of player
     */
    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    /**
     * @return list of player wagers
     */
    public List<Wager> getWagers() {
        return wagers;
    }

    /**
     * @return player name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name of player
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return account number
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * @param accountNumber of player
     */
    public void setAccountNumber(final String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * @return player birth date
     */
    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth of player
     */
    public void setDateOfBirth(final LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return String representation
     */
    @Override
    public String toString() {
        return "Player{"
                + "name='" + name + '\''
                + ", accountNumber='" + accountNumber + '\''
                + ", balance=" + balance
                + ", currency='" + currency + '\''
                + ", dateOfBirth=" + dateOfBirth + '}';
    }

    public static class Builder {
        /**
         * player name.
         */
        private String name;
        /**
         * player account number.
         */
        private String accountNumber;
        /**
         * player balance.
         */
        private long balance;
        /**
         * player currency.
         */
        private String currency;
        /**
         * player date of birth.
         */
        private LocalDate dateOfBirth;

        /**
         * @param name of player
         * @return builder object
         */
        public Builder withName(final String name) {
            this.name = name;
            return this;
        }

        /**
         * @param accountNumber of player
         * @return builder object
         */
        public Builder withAccountNumber(final String accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        /**
         * @param balance of player
         * @return builder object
         */
        public Builder withBalance(final long balance) {
            this.balance = balance;
            return this;
        }

        /**
         * @param currency of player
         * @return builder object
         */
        public Builder withCurrency(final String currency) {
            this.currency = currency;
            return this;
        }

        /**
         * @param dateOfBirth of player
         * @return builder object
         */
        public Builder withDateOfBirth(final String dateOfBirth) {
            try {
                this.dateOfBirth = LocalDate.parse(dateOfBirth);
            } catch (DateTimeParseException e) {
                LOG.warn("incorrect birth date");
            }
            return this;
        }

        /**
         * @return player object.
         */
        public Player build() {
            Player player = new Player();
            player.setName(name);
            player.setAccountNumber(accountNumber);
            player.setBalance(balance);
            player.setCurrency(currency);
            player.setDateOfBirth(dateOfBirth);
            return player;
        }

    }
}
