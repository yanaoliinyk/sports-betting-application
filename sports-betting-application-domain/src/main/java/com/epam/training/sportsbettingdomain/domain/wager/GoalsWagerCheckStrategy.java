package com.epam.training.sportsbettingdomain.domain.wager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.training.sportsbettingdomain.domain.outcome.GoalsOutcome;
import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;

public class GoalsWagerCheckStrategy extends WagerCheckStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(GoalsWagerCheckStrategy.class);

    /**
     * @param wager to check
     * @return is wager winning
     */
    @Override
    public boolean isWagerWin(final Wager wager) {
        LOG.info("start checking if wager wins");
        Outcome wagerOutcome = wager.getOutcomeOdd().getOutcome();
        GoalsOutcome resultOutcome = wager.getOutcomeOdd().getOutcome()
                .getBet().getSportEvent().getResult().getGoalsOutcome();
        if (!(wagerOutcome instanceof GoalsOutcome)) {
            LOG.error("Incorrect data");
            return false;
        }
        return ((GoalsOutcome) wagerOutcome).getGoals()
                == resultOutcome.getGoals();

    }
}
