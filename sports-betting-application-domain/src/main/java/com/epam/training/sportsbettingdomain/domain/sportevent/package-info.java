/**
 * Contains all domain classes connected with SportEvent:
 * *SportEvent class,
 * * *FootballSportEvent class,
 * * *TennisSportEvent class,
 * *Result class.
 */
package com.epam.training.sportsbettingdomain.domain.sportevent;
