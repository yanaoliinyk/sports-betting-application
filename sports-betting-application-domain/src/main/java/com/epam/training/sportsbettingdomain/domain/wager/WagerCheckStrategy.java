package com.epam.training.sportsbettingdomain.domain.wager;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@JsonIgnoreType
public abstract class WagerCheckStrategy {
    /**
     * @param wager to check
     * @return is wager winning
     */
    public abstract boolean isWagerWin(Wager wager);
}
