package com.epam.training.sportsbettingdomain.domain.wager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbettingdomain.domain.outcome.WinnerOutcome;


public class WinnerWagerCheckStrategy extends WagerCheckStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(WinnerWagerCheckStrategy.class);

    /**
     * @param wager to check
     * @return is wager winning
     */
    @Override
    public boolean isWagerWin(final Wager wager) {
        LOG.info("start checking if wager wins");
        Outcome wagerOutcome = wager.getOutcomeOdd().getOutcome();
        WinnerOutcome resultOutcome = wager.getOutcomeOdd().getOutcome()
                .getBet().getSportEvent().getResult().getWinnerOutcome();
        if (!(wagerOutcome instanceof WinnerOutcome)) {
            LOG.error("Incorrect data");
            return false;
        }
        return ((WinnerOutcome) wagerOutcome).getWinner()
                .equals(resultOutcome.getWinner());
    }
}
