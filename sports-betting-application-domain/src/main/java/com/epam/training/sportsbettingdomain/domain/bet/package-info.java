/**
 * Contains all domain classes connected with Bet:
 * *Bet class;
 * *BetType enum.
 */
package com.epam.training.sportsbettingdomain.domain.bet;
