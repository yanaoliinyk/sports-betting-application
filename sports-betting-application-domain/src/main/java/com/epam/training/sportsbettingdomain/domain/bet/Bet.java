package com.epam.training.sportsbettingdomain.domain.bet;

import java.util.List;

import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbettingdomain.domain.sportevent.SportEvent;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

public class Bet {

    /**
     * bet type.
     */
    private BetType betType;
    /**
     * bet description.
     */
    private String description;
    /**
     * sport event assigned to bet.
     */
    @JsonBackReference
    private SportEvent sportEvent;
    /**
     * list of available bet outcomes.
     */
    @JsonManagedReference
    private List<Outcome> outcomes;

    /**
     * @param betType enum value
     */
    public void setBetType(final BetType betType) {
        this.betType = betType;
    }

    /**
     * @param description string
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * @return sport event assigned to bet
     */
    public SportEvent getSportEvent() {
        return sportEvent;
    }

    /**
     * @return outcomes for bet
     */
    public List<Outcome> getOutcomes() {
        return outcomes;
    }

    /**
     * @param outcomes list
     */
    public void setOutcomes(final List<Outcome> outcomes) {
        this.outcomes = outcomes;
    }

    /**
     * @return string representation
     */
    @Override
    public String toString() {
        return "Bet:" + betType
                + ", description:" + description;
    }
}
