package com.epam.training.sportsbettingdomain.domain.outcome;

import com.epam.training.sportsbettingdomain.domain.wager.WinnerWagerCheckStrategy;

public class WinnerOutcome extends Outcome {
    /**
     * winner of game.
     */
    private String winner;

    /**
     * @param winner of game
     */
    public WinnerOutcome(final String winner) {
        this.winner = winner;
    }

    /**
     * default constructor for jackson.
     */
    public WinnerOutcome() {
        super();
    }

    /**
     * @return winner of game
     */
    public String getWinner() {
        return winner;
    }

    /**
     * @return String representation
     */
    @Override
    public String toString() {
        return "winner='" + winner + "'";
    }

    /**
     * set strategy to check if outcome wins according to bet type.
     */
    @Override
    public void setWagerCheckStrategy() {
        setWagerCheckStrategy(new WinnerWagerCheckStrategy());
    }
}
