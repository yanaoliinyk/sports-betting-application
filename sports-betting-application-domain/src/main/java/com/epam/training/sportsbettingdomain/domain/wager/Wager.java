package com.epam.training.sportsbettingdomain.domain.wager;

import com.epam.training.sportsbettingdomain.domain.outcome.OutcomeOdd;
import com.epam.training.sportsbettingdomain.domain.user.Player;

public final class Wager {
    /**
     * player assigned to wager.
     */
    private Player player;
    /**
     * outcome odd assigned to wager.
     */
    private OutcomeOdd outcomeOdd;
    /**
     * money amount of wager.
     */
    private long amount;
    /**
     * player currency.
     */
    private String currency;
    /**
     * is wager processed already.
     */
    private boolean processed;
    /**
     * is wager winning.
     */
    private boolean win;
    /**
     * profit that user gets after betting.
     */
    private long profit;

    /**
     * default constructor setting default values to boolean params.
     */
    private Wager() {
        processed = false;
        win = false;
    }

    /**
     * @return profit
     */
    public long getProfit() {
        return profit;
    }

    /**
     * @param profit that user gets after betting.
     */
    public void setProfit(final long profit) {
        this.profit = profit;
    }

    /**
     * @return player assigned to wager
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * @param player assigned to wager
     */
    private void setPlayer(final Player player) {
        this.player = player;
    }

    /**
     * @return outcome odd assigned to wager.
     */
    public OutcomeOdd getOutcomeOdd() {
        return outcomeOdd;
    }

    /**
     * @param outcomeOdd assigned to wager.
     */
    private void setOutcomeOdd(final OutcomeOdd outcomeOdd) {
        this.outcomeOdd = outcomeOdd;
    }

    /**
     * @return money put on wager.
     */
    public long getAmount() {
        return amount;
    }

    /**
     * @param amount put on wager.
     */
    private void setAmount(final long amount) {
        this.amount = amount;
    }

    /**
     * @return currency of wager.
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency of wager.
     */
    private void setCurrency(final String currency) {
        this.currency = currency;
    }

    /**
     * @return true if wager processed, false otherwise
     */
    public boolean isProcessed() {
        return processed;
    }

    /**
     * @param processed boolean
     */
    public void setProcessed(final boolean processed) {
        this.processed = processed;
    }

    /**
     * @return true if wager is winning, false otherwise.
     */
    public boolean isWin() {
        return win;
    }

    /**
     * @param win boolean
     */
    public void setWin(final boolean win) {
        this.win = win;
    }

    public static class Builder {
        /**
         * player assigned to wager.
         */
        private Player player;
        /**
         * outcome odd assigned to wager.
         */
        private OutcomeOdd outcomeOdd;
        /**
         * money amount of wager.
         */
        private long amount;
        /**
         * player currency.
         */
        private String currency;

        /**
         * @param player assigned to wager
         * @return builder object
         */
        public Builder ofPlayer(final Player player) {
            this.player = player;
            return this;
        }

        /**
         * @param outcomeOdd for wager
         * @return builder object
         */
        public Builder withOutcomeOdd(final OutcomeOdd outcomeOdd) {
            this.outcomeOdd = outcomeOdd;
            return this;
        }

        /**
         * @param amount for wager
         * @return builder object
         */
        public Builder withAmount(final long amount) {
            this.amount = amount;
            return this;
        }

        /**
         * @param currency for wager
         * @return builder object
         */
        public Builder withCurrency(final String currency) {
            this.currency = currency;
            return this;
        }

        /**
         * @return wager constructed object
         */
        public Wager build() {
            Wager wager = new Wager();
            wager.setPlayer(player);
            wager.setOutcomeOdd(outcomeOdd);
            wager.setAmount(amount);
            wager.setCurrency(currency);
            return wager;
        }
    }
}
