package com.epam.training.sportsbettingdomain.domain.outcome;

import com.epam.training.sportsbettingdomain.domain.wager.GoalsWagerCheckStrategy;

public class GoalsOutcome extends Outcome {
    /**
     * all goals during game.
     */
    private int goals;

    /**
     * constructor with no params needed for Jackson.
     */
    public GoalsOutcome() {
        super();
    }

    /**
     * @param goals number
     */
    public GoalsOutcome(final int goals) {
        this.goals = goals;
    }

    /**
     * @return goals number.
     */
    public int getGoals() {
        return goals;
    }

    /**
     * @return String representation.
     */
    @Override
    public String toString() {
        return "goals=" + goals;
    }

    /**
     * set goals wager check strategy.
     */
    @Override
    public void setWagerCheckStrategy() {
        setWagerCheckStrategy(new GoalsWagerCheckStrategy());
    }
}
