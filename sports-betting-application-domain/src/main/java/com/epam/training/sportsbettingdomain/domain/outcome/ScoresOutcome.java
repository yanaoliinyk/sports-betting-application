package com.epam.training.sportsbettingdomain.domain.outcome;

import com.epam.training.sportsbettingdomain.domain.wager.ScoreWagerCheckStrategy;

public class ScoresOutcome extends Outcome {
    /**
     * player name, on whose score customer bets.
     */
    private String playerName;
    /**
     * score of player.
     */
    private int score;

    /**
     * @param playerName on who to bet
     * @param score      of player to bet
     */
    public ScoresOutcome(final String playerName, final int score) {
        this.playerName = playerName;
        this.score = score;
    }

    /**
     * default constructor for jackson.
     */
    public ScoresOutcome() {
        super();
    }


    /**
     * @return name of player on who customer bets.
     */
    public String getPlayerName() {
        return playerName;
    }

    /**
     * @return score of player
     */
    public int getScore() {
        return score;
    }

    /**
     * @return String representation
     */
    @Override
    public String toString() {
        return "player: '" + playerName + '\''
                + ", score=" + score;
    }

    /**
     * set strategy to check if outcome wins.
     */
    @Override
    public void setWagerCheckStrategy() {
        setWagerCheckStrategy(new ScoreWagerCheckStrategy());
    }
}
