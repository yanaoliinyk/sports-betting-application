package com.epam.training.sportsbettingdomain.domain.wager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.training.sportsbettingdomain.domain.outcome.Outcome;
import com.epam.training.sportsbettingdomain.domain.outcome.ScoresOutcome;

public class ScoreWagerCheckStrategy extends WagerCheckStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(ScoreWagerCheckStrategy.class);

    /**
     * @param wager to check
     * @return is wager winning
     */
    @Override
    public boolean isWagerWin(final Wager wager) {
        LOG.info("start checking if wager wins");
        Outcome wagerOutcome = wager.getOutcomeOdd().getOutcome();
        if (!(wagerOutcome instanceof ScoresOutcome)) {
            LOG.error("Incorrect data");
            return false;
        }
        ScoresOutcome resultOutcome = wager.getOutcomeOdd().getOutcome()
                .getBet().getSportEvent().getResult()
                .getScoresOutcomes().stream()
                .filter(o -> o.getPlayerName().equals(
                        ((ScoresOutcome) wagerOutcome).getPlayerName()))
                .findFirst().orElse(null);
        if (resultOutcome == null) {
            LOG.error("incorrect data");
            return false;
        }
        return ((ScoresOutcome) wagerOutcome).getScore()
                == resultOutcome.getScore();
    }
}
