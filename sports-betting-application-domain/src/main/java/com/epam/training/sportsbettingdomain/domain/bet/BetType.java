package com.epam.training.sportsbettingdomain.domain.bet;

public enum BetType {
    /**
     * bet on concrete player's score.
     */
    SCORES_BET,

    /**
     * bet on winner.
     */
    WINNER_BET,

    /**
     * bet on goals number during whole game.
     */
    GOALS_BET
}
