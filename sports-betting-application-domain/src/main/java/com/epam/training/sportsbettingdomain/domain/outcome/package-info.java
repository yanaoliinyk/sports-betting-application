/**
 * Contains all domain classes connected with Outcome:
 * *Outcome class,
 * * *ScoresOutcome class,
 * * *WinnerOutcome class,
 * * *GoalsOutcome class,
 * *OutcomeOdd class.
 */
package com.epam.training.sportsbettingdomain.domain.outcome;
