package com.epam.training.sportsbettingdomain.domain.sportevent;

import java.time.LocalDateTime;
import java.util.List;

import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = FootballSportEvent.class, name =
                "com.epam.training.sportsbettingdomain."
                        + "domain.sportevent.FootballSportEvent"),
        @JsonSubTypes.Type(value = TennisSportEvent.class, name =
                "com.epam.training.sportsbettingdomain."
                        + "domain.sportevent.TennisSportEvent")})
public abstract class SportEvent {

    /**
     * sport event title.
     */
    private String title;
    /**
     * time of event start.
     */
    private LocalDateTime start;
    /**
     * time of sport event end.
     */
    private LocalDateTime end;
    /**
     * list of available bets.
     */
    @JsonManagedReference
    private List<Bet> bets;
    /**
     * sport event result.
     */
    private Result result;

    /**
     * @return type of this class for jackson
     */
    @JsonProperty("type")
    public String getClassName() {
        return this.getClass().getName();
    }

    /**
     * @return sport event title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return time of event start.
     */
    public LocalDateTime getStart() {
        return start;
    }

    /**
     * @return time of sport event end.
     */
    public LocalDateTime getEnd() {
        return end;
    }

    /**
     * @return list of available bets.
     */
    public List<Bet> getBets() {
        return bets;
    }

    /**
     * @return sport event result.
     */
    public Result getResult() {
        return result;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
