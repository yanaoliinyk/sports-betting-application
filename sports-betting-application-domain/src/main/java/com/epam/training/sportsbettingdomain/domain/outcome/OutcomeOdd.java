package com.epam.training.sportsbettingdomain.domain.outcome;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class OutcomeOdd {
    /**
     * outcome assigned to this odd.
     */
    @JsonBackReference
    private Outcome outcome;
    /**
     * valid from date.
     */
    private LocalDateTime from;
    /**
     * valid to date.
     */
    private LocalDateTime to;
    /**
     * odd value.
     */
    private double odd;

    /**
     * @return outcome assigned to this odd.
     */
    public Outcome getOutcome() {
        return outcome;
    }

    /**
     * @param outcome object
     */
    public void setOutcome(final Outcome outcome) {
        this.outcome = outcome;
    }

    /**
     * @return valid to date.
     */
    public LocalDateTime getTo() {
        return to;
    }

    /**
     * @param to which date odd is valid
     */
    public void setTo(final LocalDateTime to) {
        this.to = to;
    }

    /**
     * @return odd value
     */
    public double getOdd() {
        return odd;
    }

    /**
     * @param odd value
     */
    public void setOdd(final double odd) {
        this.odd = odd;
    }

    /**
     * @return valid from date.
     */
    public LocalDateTime getFrom() {
        return from;
    }

    /**
     * @param from which time odd is valid
     */
    public void setFrom(final LocalDateTime from) {
        this.from = from;
    }
}
