package com.epam.training.sportsbettingdomain.domain.sportevent;

public class FootballSportEvent extends SportEvent {
    /**
     * @return Spring representation
     */
    @Override
    public String toString() {
        return "Football sport event: "
                + "title='" + getTitle() + '\''
                + ", start=" + getStart()
                + ", end=" + getEnd() + '}';
    }
}
