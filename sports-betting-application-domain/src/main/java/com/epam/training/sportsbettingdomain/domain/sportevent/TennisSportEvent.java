package com.epam.training.sportsbettingdomain.domain.sportevent;

public class TennisSportEvent extends SportEvent {
    /**
     * @return Spring representation
     */
    @Override
    public String toString() {
        return "Tennis sport event: "
                + "title='" + getTitle() + '\''
                + ", start=" + getStart() + ", end=" + getEnd() + '}';
    }
}
