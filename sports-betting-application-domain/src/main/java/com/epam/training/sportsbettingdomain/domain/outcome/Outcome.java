package com.epam.training.sportsbettingdomain.domain.outcome;

import java.util.List;

import com.epam.training.sportsbettingdomain.domain.bet.Bet;
import com.epam.training.sportsbettingdomain.domain.wager.WagerCheckStrategy;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ScoresOutcome.class, name =
                "com.epam.training.sportsbettingdomain.domain.outcome.ScoresOutcome"),
        @JsonSubTypes.Type(value = WinnerOutcome.class, name =
                "com.epam.training.sportsbettingdomain.domain.outcome.WinnerOutcome"),
        @JsonSubTypes.Type(value = GoalsOutcome.class, name =
                "com.epam.training.sportsbettingdomain.domain.outcome.GoalsOutcome")})
public abstract class Outcome {
    /**
     * strategy that checks that wager is winning according to bet type.
     */
    private WagerCheckStrategy wagerCheckStrategy;
    /**
     * bet assigned to this outcome.
     */
    @JsonBackReference
    private Bet bet;
    /**
     * outcome odds for outcome.
     */
    @JsonManagedReference
    private List<OutcomeOdd> outcomeOdds;

    /**
     * no params constructor, that sets strategy for each outcome subclass.
     */
    public Outcome() {
        setWagerCheckStrategy();
    }

    /**
     * sets strategy for each outcome subclass.
     */
    public abstract void setWagerCheckStrategy();

    /**
     * @return strategy that checks that wager is winning according to bet type.
     */
    public WagerCheckStrategy getWagerCheckStrategy() {
        return wagerCheckStrategy;
    }

    /**
     * @return bet assigned to outcome
     */
    public Bet getBet() {
        return bet;
    }

    /**
     * @param bet object
     */
    public void setBet(final Bet bet) {
        this.bet = bet;
    }

    /**
     * @return odds list
     */
    public List<OutcomeOdd> getOutcomeOdds() {
        return outcomeOdds;
    }

    /**
     * @param outcomeOdds list
     */
    public void setOutcomeOdds(final List<OutcomeOdd> outcomeOdds) {
        this.outcomeOdds = outcomeOdds;
    }

    public void setWagerCheckStrategy(WagerCheckStrategy wagerCheckStrategy) {
        this.wagerCheckStrategy = wagerCheckStrategy;
    }

    /**
     * @return object type for jackson
     */
    @JsonProperty("type")
    public String getClassName() {
        return this.getClass().getName();
    }
}
